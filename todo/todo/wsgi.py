def app (environ, start_fn):
    start_fn('200 OK', [('Content-Type', 'test/plan')])
    return [b"Hello World"] # python3
    #return ["Hello World"] # python2

def application(env, start_response):
    start_response('200 OK', [('Content-Type','text/html')])
    return [b"WSGI module is running!\n"] # python3
    #return ["WSGI module is running!\n"] # python2

