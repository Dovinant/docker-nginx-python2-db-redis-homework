# Найденные проблемы и их решения во второй части задания 8.10

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## uWSGI ./python_plugin.so error

При сборке выскакивает ошибка:

> !!! UNABLE to load uWSGI plugin: ./python_plugin.so: cannot open shared object file: No such file or directory !!!

Связано это с тем, что uWSGI устанавливается командой `pip install uwsgi`, а не `$ apt-get install uwsgi-plugin-python3`

### Решение
В файлах  `web/uwsgi.ini` и `consumers/uwsgi.ini` комментируем строку `plugin = python`
[Подробнее...](https://stackoverflow.com/questions/15936413/pip-installed-uwsgi-python-plugin-so-error)

## Неверные пути

В докер контйнерах рабочий каталог располжен по адресу `/var/www/todo`. Путь к нему прописан в файлах
`nginx/sites/todo.conf`, `consumers/supervisor/todo.conf`, `.env`. Этот каталог должен отображаться на каталог хоста.
В задании использутся среда разработки [Laradoc](http://laradock.io/), в файле `docker-compose.yml` указаны переменные,
преременные эти определены в файле `.env`.

### Решение
В файле `.env` определяем переменные:

`APP_CODE_TODO_PATH_HOST=./todo` - локальная папка, находится внутри проекта\
`APP_CODE_TODO_PATH_CONTAINER=/var/www/todo` - папка в контейнере

и проверяем эти переменные в файле `docker-compose.yml`

## ImportError: No module named todo.wsgi

Не получается импортировать питоновский модуль `wsgi` из package `todo`. 
Модуль указан в файлах `./web/uwsgi.ini` и `./consumers/uwsgi.ini` в директиве `module = package.module:function`.

### Решение
Чтобы импорт прошёл успешно в рабочем каталоге должен быть питоновский package, т.е. каталог `todo` внутри которого должны быть файлы `wsgi.py` и `__init__.py`.
Файл `uwsgi.ini` содержит скрипт на Python, файл `__init__.py` может быть пустым [подробнее...](https://habr.com/ru/company/skillfactory/blog/683744/#init)

## Дополнительные материалы
- [Django на production. uWSGI + nginx. Подробное руководство](https://habr.com/ru/post/226419/)
- [Развёртывание UWSGI+NGINX+DJANGO на Docker (Youtube)](https://www.youtube.com/watch?v=GE4k7eqstEo)
- [Run Python Web Application in Docker using Nginx and uWsgi](https://www.novixys.com/blog/python-web-application-docker-nginx-uwsgi/)
- [Шпаргалка по Laradock](https://modern-develop.ru/blogs/shpargalka-po-laradock)
- [Understanding "VOLUME" instruction in DockerFile](https://stackoverflow.com/questions/41935435/understanding-volume-instruction-in-dockerfile)
